package com.macknighter.lexus;

/**
 * Created by rodrigo.poloni on 06/06/16.
 */
public class SubReddit {

    private String name;
    private String address;
    private boolean subscribed;
    private String displayName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isSubscribed() {
        return subscribed;
    }

    public void setSubscribed(boolean subscribed) {
        this.subscribed = subscribed;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }


    public SubReddit(String name, String address, boolean subscribed, String displayName) {
        this.name = name;
        this.address = address;
        this.subscribed = subscribed;
        this.displayName = displayName;
    }
}
