package com.macknighter.lexus;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;

public class OptionsActivity extends AppCompatActivity {

    public static final int SUBREDDITS_INDEX_GIFS = 0;
    public static final int SUBREDDITS_INDEX_EDUCATIONAL = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    public void onCheckboxClicked(View view){
        //TODO: TROCAR POR SWITCH/CASE


        boolean checked =  ((CheckBox) view).isChecked();

        if(view.getId()== findViewById(R.id.checkbox_randomGifs).getId()){
            Log.e("checkbox: ", "randomgifs");
            if(checked) {
                MainActivity.subReedits.get(SUBREDDITS_INDEX_GIFS).setSubscribed(true);
            }else{
                MainActivity.subReedits.get(SUBREDDITS_INDEX_GIFS).setSubscribed(false);
            }

        }else if(view.getId()== findViewById(R.id.checkbox_educational).getId()){
            Log.e("checkbox: ", "educational");
            if(checked) {
                MainActivity.subReedits.get(SUBREDDITS_INDEX_EDUCATIONAL).setSubscribed(true);
            }else{
                MainActivity.subReedits.get(SUBREDDITS_INDEX_EDUCATIONAL).setSubscribed(false);
            }
        }
    }

}
