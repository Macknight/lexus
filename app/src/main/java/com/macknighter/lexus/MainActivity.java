package com.macknighter.lexus;

import android.app.Activity;

import android.content.Intent;

import android.graphics.Movie;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;


import android.os.PersistableBundle;
import android.util.Log;

import android.view.View;


import org.apache.commons.io.IOUtils;

import java.io.InputStream;


import java.net.URL;
import java.util.ArrayList;

import java.util.List;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

public class MainActivity extends Activity {




    int currentGif ;

    private GifDrawable nextGif;

    private ArrayList<Drawable> gifList;

    GifImageView mainGif;

    public static List<SubReddit> subReedits;

    List<Post> posts;

    static int taskCounter = 0;

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        setSubReddits();


        //pegando referencia ao displayer dos gifs
        mainGif = (GifImageView) findViewById(R.id.main_gif);
        //inicializando o arraylist que vai segurar os gifs carregados
        gifList = new ArrayList<Drawable>();
        //adiciona o primeiro gif na lista
        gifList.add((Drawable) mainGif.getDrawable());
        //pegando o iterador para ter acesso ao getNext() e getPrevious()
        posts = new ArrayList<Post>();






        currentGif = 0;

        //Populando o array posts
         fecthPostsAndStartLoading();


    }

    /** POPULATE POSTS LIST**/
    /** CALLS GIFLOADER **/
    private void fecthPostsAndStartLoading() {
        new Thread(){
            public void run(){
               for (SubReddit sr : subReedits) { //para cada sub
                    if(sr.isSubscribed()){
                       PostsHolder postsHolder = new PostsHolder(sr.getAddress());

                        if(posts!=null && posts.isEmpty()) {
                            posts = postsHolder.fetchPosts();
                        }else{
                            posts.addAll(postsHolder.fetchPosts());
                        }


                    }
                }

                new DonwloadGif().execute(curarUrl(posts.get(0).url));

                //chama a carregador assincrono


            }
        }.start();
    }


    /** LOAD SUBREDDITS **/
    private void setSubReddits() {

        subReedits = new ArrayList<SubReddit>();
        SubReddit gifs = new SubReddit("Random Gifs","gifs",true,"Random Gifs");
        SubReddit educational = new SubReddit("Educational","EducationalGifs",true,"Educational");

        subReedits.add(gifs);
        subReedits.add(educational);


    }

    /** BOTAO PROXIMO */
    public void onClickNext(View view){
        GifImageView mainGif = (GifImageView) findViewById(R.id.main_gif);

        currentGif++; //

        if(currentGif<=gifList.size()-1) {
            mainGif.setImageDrawable(gifList.get(currentGif));
        }else{
            currentGif = gifList.size() - 1;
            //TODO: AVISAR USUARIO QUE NÃO TEM MAIS
        }
        Log.e("currentGif:", currentGif + "");
    }

    /** BOTAO ANTERIOR */
    public void onClickPrevious(View view){
        GifImageView mainGif = (GifImageView) findViewById(R.id.main_gif);

        currentGif--;
        //se o contador for pra baixo do 0 deixa ele no 0.
        if(currentGif>=0) {
            mainGif.setImageDrawable(gifList.get(currentGif));

        }else{
            currentGif = 0;
            //TODO: AVISAR USUARIO QUE NÃO TEM MAIS
        }
        Log.e("currentGif:", currentGif + "");
    }


    /** BOTAO OPTIONS **/
    public void onClickOptions(View view){
        Intent optIntent = new Intent(this, OptionsActivity.class);
        startActivity(optIntent);
    }

    /** GIFV -> GIF **/
    public String curarUrl(String url){
        String gifUrlCurada= "";
        if(url.endsWith("gif")){
            gifUrlCurada=url;

        }else if(url.endsWith("gifv")){
            gifUrlCurada=url.replace("gifv","gif");
        }else{
            Log.e("Its not a gif tought",url);
        }
        return gifUrlCurada;
    }

    private class DonwloadGif extends AsyncTask<String,Void,GifDrawable>{

                    @Override
                    protected GifDrawable doInBackground(String... params) {
                        GifDrawable newGif = null;
                        try {

                            Log.e("TaskCounter:", taskCounter + "");
                            Log.e("CurrentGif: ", params[0]);

                            taskCounter++;



                            URL url = new URL(params[0]);

                            InputStream is = url.openStream();

                            byte[] bytes = IOUtils.toByteArray(is);


                            newGif = new GifDrawable(bytes);

                            Log.e("GifSize: " , newGif.getAllocationByteCount()+ "");

                            is.close();

                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e("GIF CANNOT BE LOADED: ", params[0]);
                        }
                        return newGif;
                    }


                    @Override
                    protected void onPostExecute(GifDrawable gifDrawable) {
                        if(gifDrawable!=null) {
                            gifList.add(gifDrawable);
                            Log.e("GifAdded, total gifs:", gifList.size() + "");

            }else{
                Log.e("GifMissed from task:", taskCounter + "" );
            }
            if(!posts.isEmpty()){
                posts.remove(0);
                if(!posts.isEmpty()){
                    new DonwloadGif().execute(curarUrl(posts.get(0).url));
                }
            }


        }
    }





}
